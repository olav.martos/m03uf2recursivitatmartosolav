﻿/*
 * AUTHOR: Olav Martos
 * DATE: 18/01/2023
 * DESCRIPTION: Conjunt de exercicis per practicar la recursivitat
 */


using System;

namespace Recursivitat
{
    class Recursivitat
    {
        static void Main()
        {
            var menu = new Recursivitat();
            menu.Menu();
        }

        /*Menu de exercicis*/
        public void Menu()
        {
            var option = "";
            string[] exercise = { "Sortir del menu", "Factorial", "Doble factorial", "Nombre de dígits", "Nombres creixents", "Reducció de dígits", "Seqüència d’asteriscos", "Primers perfectes", "Torres de Hanoi" };
            do
            {
                Console.Clear();
                showExercise(exercise);
                Console.Write("Escull una opcio: ");

                option = Console.ReadLine();
                
                switch (option)
                {
                    case "1":
                        Console.Clear();
                        programExecuted(exercise, option);
                        Factorial();
                        break;
                    case "2":
                        Console.Clear();
                        programExecuted(exercise, option);
                        DoubleFactorial();
                        break;
                    case "3":
                        Console.Clear();
                        programExecuted(exercise, option);
                        DigitNumber();
                        break;
                    case "4":
                        Console.Clear();
                        programExecuted(exercise, option);
                        GrowingNumber();
                        break;
                    case "5":
                        Console.Clear();
                        programExecuted(exercise, option);
                        DigitReduction();
                        break;
                    case "6":
                        Console.Clear();
                        programExecuted(exercise, option);
                        AsteriskSequence();
                        break;
                    case "7":
                        Console.Clear();
                        programExecuted(exercise, option);
                        PerfectNumber();
                        break;
                    case "8":
                        Console.Clear();
                        programExecuted(exercise, option);
                        HanoiTower();
                        break;
                    case "0":
                        Console.Clear();
                        endExercise();
                        Environment.Exit(0);
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opció incorrecta!");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
                Console.ReadLine();
            } while (option != "0");

        }

        /*Mostra tots el exercicis d'aquesta solució*/
        public void showExercise(string[] ex)
        {
            for (int i = 0; i < ex.Length; i++) Console.WriteLine($"{i}. - {ex[i]}");
        }

        /*Programa que ens permet sapiguer quan ha finalitzat un exercici*/
        public void endExercise()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;
            return;
        }

        /*Programa que ens permet sapiguer en quin exercici ens trobem en aquell moment*/
        public void programExecuted(string[] exercise, string option)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            int index = Convert.ToInt32(option);
            Console.Write("Programa ejecutado: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{exercise[index]}");
            Console.ResetColor();
        }



        /*Escriviu una funció que retorni n! Resoleu aquest problema recursivament.*/
        public void Factorial()
        {
            Console.Write("Numero a obtenir el seu factorial: ");
            int num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{num}! = {FactorialCalculate(num)}");
            endExercise();
        }

        /*Part de Factorial*/
        int FactorialCalculate(int n)
        {
            if (n > 1) return n *= FactorialCalculate(n - 1);
            return 1;
        }



        /*
         * Escriviu una funció recursiva que retorni n!!. Recordeu que n!! = n × (n − 2) × (n − 4)
         * × ...
         * Exemple: 9!! = 9 x 7 x 5 x 3 x 1
         */
        public void DoubleFactorial()
        {
            Console.Write("Numero a obtenir el seu factorial: ");
            int num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{num}!! = {DoubleFactorialCalculate(num)}");
            endExercise();
        }

        /*Part de DoubleFactorial*/
        int DoubleFactorialCalculate(int n)
        {
            if (n > 1) return n *= DoubleFactorialCalculate(n - 2);
            return 1;
        }



        /*Escriviu una funció recursiva que retorni el nombre de dígits de n.*/
        void DigitNumber()
        {
            Console.Write("Escriu el numero a tornar el nombre de digits: ");
            int num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"El numero de xifres de {num} es {DigitNumberCalculate(num)}");
            endExercise();
        }

        /*Part de DigitNumber*/
        int DigitNumberCalculate(int num)
        {
            if (num < 10) return 1;
            else return 1 + DigitNumberCalculate(num / 10);
        }



        /*
         * Escriviu una funció recursiva que ens indiqui si un nombre és creixent o no. Un
         * nombre és creixent si cada dígit és més petit o igual que el que està a la seva dreta.
         */
        void GrowingNumber()
        {
            Console.Write("Introdueix el numero: ");
            int num = Convert.ToInt32(Console.ReadLine());
            bool result = GrowingNumberCalc(num);
            Console.WriteLine(result);
            endExercise();
        }

        /*Part de GrowingNumber*/
        bool GrowingNumberCalc(int num)
        {
            if (num < 10) return true;
            else
            {
                int lastDigit = num % 10;
                int restOfNum = num / 10;
                int secondLastDigit = restOfNum % 10;

                if (lastDigit < secondLastDigit) return false;
                else  return GrowingNumberCalc(restOfNum);
            }
        }



        /*
         * Feu una funció recursiva que, donat un natural x, retorni la reducció dels seus dígits.
         * Direm que reduir els dígits d’un nombre consisteix a calcular la suma dels seus
         * dígits. Si la suma és un dígit, aquest ja és el resultat. Altrament, es torna a aplicar el
         * mateix procés a la suma obtinguda, fins a tenir un sol dígit.
         */
        void DigitReduction()
        {
            Console.Write("Introdueix el numero: ");
            int num = Convert.ToInt32(Console.ReadLine());
            int result = DigitReductionCalc(num);
            Console.WriteLine($"Resultat: {result}");
            endExercise();
        }

        /*Part de DigitReduction*/
        int DigitReductionCalc(int num)
        {
            if (num < 10) return num;
            else return DigitReductionCalc(num % 10 + num/10);
        }



        /*
         * Feu un programa recursiu que llegeixi un natural n i que escrigui 2n − 1 barres amb
         * asteriscos seguint el patró que es pot deduir dels exemples.
         */
        void AsteriskSequence()
        {
            Console.Write("Introdueix el numero: ");
            int num = Convert.ToInt32(Console.ReadLine());
            while (num <= 0)
            {
                Console.WriteLine("Numero incorrecte, ha de ser mes gran que 0");
                Console.Write("Introdueix el numero correcte: ");
                num = Convert.ToInt32(Console.ReadLine());
            }
            AsteriskSequenceCalc(num);
            endExercise();
        }

        /*Part de AsteriskSequence*/
        int AsteriskSequenceCalc(int num)
        {
            if (num>0)
            {
                AsteriskSequenceCalc(num - 1);
                printAsteriskSequence(num);
                return AsteriskSequenceCalc(num - 1);
            }
            return 1;
        }

        /*Part de AsteriskSequence*/
        void printAsteriskSequence(int num)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            for (int i = 0; i < num; i++) Console.Write("*");
            Console.WriteLine();
            Console.ResetColor();
        }



        /*
         * Escriviu una funció recursiva que ens indiqui si un nombre és primer perfecte o no.
         * Un primer perfecte ho és si totes les sumes dels seus dígits fins aquesta ser un
         * nombre d’un dígit donen un nombre primer. Per exemple, 977 és un primer perfecte,
         * perquè tant 977, com 9 + 7 + 7 = 23, com 2 + 3 = 5, com 5, ..., són tots nombres
         */
        void PerfectNumber()
        {
            Console.Write("Escriu el numero ha examinar: ");
            int num = Convert.ToInt32(Console.ReadLine());
            PerfectNumberCalc(num);
            endExercise();
        }

        /*Part de PerfectNumber*/
        bool IsPrime(int num)
        {
            for (int i = 2; i < num; i++)
            {
                if ((num % i) == 0) return false;
            }

            return true;
        }

        /*Part de PerfectNumber*/
        bool PerfectNumberCalc(int num)
        {
            if (DigitNumberCalculate(num) == 1)
            {
                if (IsPrime(num) == true) Console.WriteLine(true);
                else Console.WriteLine(false);

                return false;
            }
            if (IsPrime(num))
            {
                int sum = 0;
                while (num > 0)
                {
                    sum += num % 10;
                    num /= 10;
                }
                return PerfectNumberCalc(sum);
            }
            else
            {
                if (IsPrime(num) == true) Console.WriteLine(true);
                else Console.WriteLine(false);
                return false;
            }
        }



        /*
         * Les Torres de Hanoi és un joc que consisteix en tres pals i n discos de mides
         * diferents que es poden fer lliscar a cada pal. El joc comença amb els discos en el
         * pal de l’esquerra apilats en ordre, amb el més gran al fons. L’objectiu del joc és
         * moure tots els discos del pal de l’esquerra (A) al pal de la dreta (C), utilitzant el pal
         * del mig (B) com a auxiliar. Els moviments han de seguir les regles següents:
         * * Només es pot moure un sol disc a cada pas.
         * * Cada moviment consisteix a agafar el disc superior d’un dels tres pals i moure’l a sobre de tots els discos d’un altre pal.
         * * No es pot col·locar cap disc a sobre d’un de més petit.
         * Feu un programa que resolgui el joc de les torres de Hanoi, de manera que es facin
         * el mínim nombre de moviments possibles.
         */
        void HanoiTower()
        {
            char towerA = 'A', towerB = 'B', towerC = 'C';
            Console.Write("Quants discos vols moure: ");
            int num = int.Parse(Console.ReadLine());
            while (num < 1)
            {
                Console.WriteLine("El numero de discos no pot ser 0 o negatiu");
                Console.Write("Torni a posar un numero: ");
                num = int.Parse(Console.ReadLine());
            }


            /*
             * S'han realitzat proves amb diferents numeros de discos a moure i quant de temps tarda en terminar
             * Aquests son els numeros:
             * 13 discos (Maxim numero de discos segons Geogebra) = 1,8828718 seg         https://www.geogebra.org/m/NqyWJVra
             * 14 discos = 2,2944221 seg
             * 15 discos = 2,942507 seg
             * 16 discos = 3,7806447 seg
             * 17 discos = 5,6763574 seg
             * 18 discos = 9,777126 seg
             * 19 discos = 16,8268383 seg
             * 20 discos = 32,9494355 seg
             * 21 discos = 63,8321464 seg = 1,063869106667 min
             * 22 discos = 131,0817336 seg = 2,18469556 min
             * 
             * Per tant la meva recomenació es limitar el numero a posar a 18. Si vosté no vol aquest limit, pot comentar el while a continuació.             
             */

            while (num > 18)
            {
                Console.WriteLine("El limit de discos es 18.");
                Console.Write("Torni a posar un numero: ");
                num = int.Parse(Console.ReadLine());
            }
            HanoiTowerMovement(num, towerA, towerB, towerC);
            endExercise();
        }

        /*Part de HanoiTower*/
        void HanoiTowerMovement(int num, char towerA, char towerB, char towerC)
        {
            if (num > 0)
            {
                HanoiTowerMovement(num - 1, towerA, towerC, towerB);
                Console.WriteLine($"{towerA} => {towerC}");
                HanoiTowerMovement(num - 1, towerB, towerA, towerC);
            }
        }

    }
}
